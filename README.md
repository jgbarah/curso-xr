# Curso XR

Materiales para un curso de XR (incluyendo VR, AR), con un gran componente práctico, basado en A-Frame.

* Transparencias: [PDF](transpas/Transpas.pdf), [ODP](transpas/Transpas.odp)

Materiales sobre A-Frame:

* [Sitio web de A-Frame](https://aframe.io)
* [A-Frame Playground](https://jgbarah.github.io/aframe-playground/)

Ejercicios con A-Frame:

* [Ejemplo de animación (1)](ejemplos-animation/rotar.html)
* [Ejemplo de animación (2)](ejemplos-animation/rotar2.html)
* [Ejemplo de AR (1)](ejemplos-ar/basic_ar.html)
* [Ejemplo de AR (2)](ejemplos-ar/basic_ar2.html)
* [Ejemplo de manos en VR (1)](ejemplos-manos/basic_hands.html)
* [Ejemplo de manos en VR (2)](ejemplos-manos/basic_hands2.html)
* [Ejemplo de física (1)](ejemplos-fisica/caida.html)
* [Ejemplo de física (2)](ejemplos-fisica/caida2.html)
* [Ejemplo de física (3)](ejemplos-fisica/caida3.html)
* [Ejemplo de figuras geométricas](ejemplos-figuras/figuras.html)
* [Ejemplo de cajas y palabras (1)](ejemplos-palabras/palabras1.html)
* [Ejemplo de cajas y palabras (2)](ejemplos-palabras/palabras2.html)
* [Ejemplo de cajas y palabras (3)](ejemplos-palabras/palabras3.html)
* [Ejemplo de cajas y palabras (4)](ejemplos-palabras/palabras4.html)

Escenas de ejemplo:

* [Museo](scenes/museum/index.html)
* [Demos de VRNetVis](https://sarehp.github.io/vrnetvis/demos/)
    * [Ejemplo traceroute](https://sarehp.github.io/vrnetvis/demos/traceroute/index-vr.html) ([fichero HTML](https://github.com/sarehp/vrnetvis/blob/main/demos/traceroute/index-vr.html))
* [Visualización de datos](scenes/babia/index.html)

Algunos ejemplos de XR en educación:

* [Immersive Learning Using Mozilla Hubs](https://eduverse.io/mozilla-hubs/)
* [Educational spaces in Hubs](https://hubs.mozilla.com/labs/exhibitions-that-educate/)

Certificados para servir HTTPS:

* [babia_cert.pem](https://jgbarah.github.io/aframe-playground/seminar-01/babia_cert.pem)
* [babia_key.pem](https://jgbarah.github.io/aframe-playground/seminar-01/babia_key.pem)

Documentación, cursitos, tutorials:

* [A-Frame Playground](https://jgbarah.github.io/aframe-playground/)


[Código fuente en GitLab](https://gitlab.com/jgbarah/curso-xr)

