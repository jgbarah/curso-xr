AFRAME.registerComponent('isin', {
    schema: {
        selector: {type: 'selectorAll', default: '.words'},
        interval: {type: 'int', default: 2000}
      },

    init: function () {
        let self = this;
        let data = this.data;

        function check_isin() {
            mesh = self.el.getObject3D('mesh');
            var bbox = new THREE.Box3().setFromObject(mesh);
            els = data.selector;

            let done = true;
            for (el of els) {
                console.log(el);
                pos = el.getAttribute('position');
                isin = bbox.containsPoint(pos);
                console.log(isin);
                if (isin) {
                    el.setAttribute('material', {'emissive': '#FFF'});
                } else {
                    el.setAttribute('material', {'emissive': '#000'});
                    done = false;
                };
            };
            if (done) {
                color = self.el.getAttribute('material').color;
                self.el.setAttribute('material', {'emissive': color});
            } else {
                self.el.setAttribute('material', {'emissive': '#000'});
            }
          };

        setInterval(check_isin, data.interval);
    }
  });